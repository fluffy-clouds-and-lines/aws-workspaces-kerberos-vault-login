module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "fcal-vault-demo"
  cidr = var.vpc_cidr

  azs             = ["euw2-az2","euw2-az3"] # AWS Workspaces is not supported in all AZs in London Region
  public_subnets  = var.public_subnet_cidrs
  private_subnets = var.private_subnet_cidrs
  intra_subnets   = var.intra_subnet_cidrs

  enable_nat_gateway = true
  single_nat_gateway = true
  enable_vpn_gateway = false

}

resource "tls_private_key" "ec2_ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "random_pet" "ssh_key" {
  length = 2
}

resource "aws_key_pair" "demo" {
  key_name   = random_pet.ssh_key.id
  public_key = tls_private_key.ec2_ssh_key.public_key_openssh
}

resource "local_file" "private_key" {
  content         = tls_private_key.ec2_ssh_key.private_key_pem
  filename        = "${path.module}/ec2-access-key-${random_pet.ssh_key.id}.pem"
  file_permission = "0600"
}

module "rdp_test_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "rdp-test"
  description = "Security group for test RDP host"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 3389
      to_port     = 3389
      protocol    = "tcp"
      description = "RDP Ports"
      cidr_blocks = var.remote_source_ip
    }
  ]

  egress_with_source_security_group_id = [
    {
      from_port                = 8200
      to_port                  = 8201
      protocol                 = "tcp"
      description              = "Vault User Ports"
      source_security_group_id = module.vault_sg.security_group_id
    },
    {
      from_port                = 22
      to_port                  = 22
      protocol                 = "tcp"
      description              = "SSH Mgmt"
      source_security_group_id = module.vault_sg.security_group_id
    }
  ]

  egress_rules = ["https-443-tcp", "http-80-tcp"]

}

data "aws_prefix_list" "s3" {
  name = "com.amazonaws.eu-west-2.s3"
}

module "vault_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "vault-clients"
  description = "Security group for Vault clients"
  vpc_id      = module.vpc.vpc_id

  ingress_with_source_security_group_id = [
    {
      from_port                = 8200
      to_port                  = 8201
      protocol                 = "tcp"
      description              = "Vault User Ports"
      source_security_group_id = module.rdp_test_sg.security_group_id
    },
    {
      from_port                = 80
      to_port                  = 80
      protocol                 = "tcp"
      description              = "CA Cert Distribution"
      source_security_group_id = module.rdp_test_sg.security_group_id
    },
    {
      from_port                = 22
      to_port                  = 22
      protocol                 = "tcp"
      description              = "SSH Mgmt"
      source_security_group_id = module.rdp_test_sg.security_group_id
    }
  ]

  egress_rules           = ["https-443-tcp", "http-80-tcp"]
  egress_prefix_list_ids = [data.aws_prefix_list.s3.id]

}

resource "aws_iam_role_policy" "vault_iam" {
  name   = "vault_iam"
  role   = aws_iam_role.example_client_instance_role.id
  policy = data.aws_iam_policy_document.vault_iam.json
}

data "aws_iam_policy_document" "vault_iam" {
  statement {
    effect  = "Allow"
    actions = ["iam:GetRole", "iam:GetUser", "iam:GetInstanceProfile"]
    resources = [
      "arn:aws:iam::*:user/*",
      "arn:aws:iam::*:role/Vault*",
    ]
  }
  statement {
    effect    = "Allow"
    actions   = ["sts:GetCallerIdentity"]
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "example_instance_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "example_client_instance_role" {
  name_prefix        = "Vault-RDP-Enrolment"
  assume_role_policy = data.aws_iam_policy_document.example_instance_role.json
}

resource "aws_iam_instance_profile" "example_instance_profile" {
  path = "/"
  role = aws_iam_role.example_client_instance_role.name
}

resource "aws_route53_zone" "private" {
  name = "fcal-vault-demo.example.com"
  vpc {
    vpc_id = module.vpc.vpc_id
  }
}