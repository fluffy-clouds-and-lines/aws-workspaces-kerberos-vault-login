#!/usr/bin/env bash
set -xe

amazon-linux-extras install epel -y
yum update -y
yum install -y krb5-workstation

PATH=$PATH:/usr/local/bin
# Grab terraform-aws-vault and 'install' vault (systemd etc)
cd /tmp
wget https://github.com/hashicorp/terraform-aws-vault/archive/v0.17.0.zip
unzip ./v0.17.0.zip
rm -f ./v0.17.0.zip
./terraform-aws-vault-0.17.0/modules/install-vault/install-vault --version 1.9.3
wget https://releases.hashicorp.com/terraform/1.1.6/terraform_1.1.6_linux_amd64.zip
unzip terraform_1.1.6_linux_amd64.zip
mv terraform /usr/local/sbin/terraform

# Generate our self-signed root CA certs and Vault Server cert / keypair
cd ./terraform-aws-vault-0.16.0/modules/private-tls-cert/
terraform init
terraform apply -var ca_public_key_file_path=/opt/vault/tls/ca.crt.pem \
                -var public_key_file_path=/opt/vault/tls/vault.crt.pem \
                -var private_key_file_path=/opt/vault/tls/vault.key.pem \
                -var owner=vault \
                -var organization_name='FCAL' \
                -var ca_common_name=infra.fluffycloudsandlines.blog \
                -var common_name=vault.${ad_fqdn} \
                -var dns_names='["vault.workspaces.${ad_fqdn}"]' \
                -var ip_addresses='["127.0.0.1"]'\
                -var validity_period_hours=24 \
                -auto-approve

# Add the Root CA into the OS trust store
cd /tmp
./terraform-aws-vault-0.16.0/modules/update-certificate-store/update-certificate-store --cert-file-path /opt/vault/tls/ca.crt.pem

# Run the Vault Config script
rm -f /opt/vault/config/default.hcl
/opt/vault/bin/run-vault --tls-cert-file /opt/vault/tls/vault.crt.pem --tls-key-file /opt/vault/tls/vault.key.pem 

# Remove consul storage backend
sed -i -e '/storage "consul"/,/}/d' /opt/vault/config/default.hcl

# Add local storage backend
cat << EOF > /opt/vault/config/fcal.hcl
storage "file" {
  path = "/opt/vault/data"
}
EOF

systemctl restart vault.service 

# Wait for vualt boot then initialise, and capture unseal keys - naughty, again
sleep 3
/opt/vault/bin/vault operator init > /opt/vault/config/init_output

export PATH=$PATH:/opt/vault/bin
export VAULT_ROOT_KEY=$(cat /opt/vault/config/init_output | grep "Root Token" | cut -d ":" -f 2 | xargs)
export VAULT_UK1_KEY=$(cat /opt/vault/config/init_output | grep "Unseal Key 1" | cut -d ":" -f 2 | xargs)
export VAULT_UK2_KEY=$(cat /opt/vault/config/init_output | grep "Unseal Key 2" | cut -d ":" -f 2 | xargs)
export VAULT_UK3_KEY=$(cat /opt/vault/config/init_output | grep "Unseal Key 3" | cut -d ":" -f 2 | xargs)
export WINDOWS_DOMAIN=${ad_fqdn}
export VAULT_FORMAT='json'

export

# Unseal our vault ready for setup
sleep 3
vault operator unseal $VAULT_UK1_KEY
vault operator unseal $VAULT_UK2_KEY
vault operator unseal $VAULT_UK3_KEY
vault login $VAULT_ROOT_KEY

vault auth enable -passthrough-request-headers=Authorization -allowed-response-headers=www-authenticate kerberos

VAULT_SVC_USER="${vault_service_account_user}"
VAULT_SVC_PASSWORD="${vault_service_account_password}"

printf "%b" "addent -password -p $VAULT_SVC_USER -k 1 -e aes256-cts-hmac-sha1-96\n$VAULT_SVC_PASSWORD\nwrite_kt vault.keytab" | ktutil

printf "%b" "read_kt vault.keytab\nlist" | ktutil

base64 vault.keytab > vault.keytab.base64

vault write auth/kerberos/config keytab=@vault.keytab.base64 service_account="$${VAULT_SVC_USER}"

vault write auth/kerberos/config/ldap \    
    binddn=$${VAULT_SVC_USER}@${kerberos_domain} \    
    bindpass=$${VAULT_SVC_PASSWORD} \    
    groupattr=sAMAccountName \    
    groupdn="${domain_dn}" \    
    groupfilter="(&(objectClass=group)(member:1.2.840.113556.1.4.1941:={{.UserDN}}))" \    
    userdn="${domain_dn}" \    
    userattr=sAMAccountName \    
    upndomain=${kerberos_domain} \    
    url=ldaps://${ad_fqdn}
    disable_fast_negotiation=true

vault secrets enable aws

vault write aws/config/root region=eu-west-2

vault write aws/roles/deploy \
    role_arns=arn:aws:iam::${account_id}:role/WorkspacesInstance \
    credential_type=assumed_role