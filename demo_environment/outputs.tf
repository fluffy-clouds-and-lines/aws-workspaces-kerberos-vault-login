output "windows_ec2_ip" {
  value = module.windows_ec2.public_ip
}

output "vault_ec2_ip" {
  value = module.vault_ec2.private_ip
}

output "windows_bastion_password" {
  value = nonsensitive(
    rsadecrypt(
      module.windows_ec2.password_data[0], tls_private_key.ec2_ssh_key.private_key_pem
    )
  )
}

output "ad_password" {
  value = nonsensitive(random_password.ad_admin.result)
}

output "workspaces_registration_code" {
  value = aws_workspaces_directory.wks_registration.registration_code
}