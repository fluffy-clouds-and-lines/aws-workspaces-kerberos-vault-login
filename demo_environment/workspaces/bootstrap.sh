#!/bin/bash

set -xe

yum install -y wget yum-utils
release=AmazonLinux
yum-config-manager --add-repo https://rpm.releases.hashicorp.com/$release/hashicorp.repo