module "vault_ec2" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 2.0"

  name           = "vault"
  instance_count = 1

  ami                    = data.aws_ami.al2.id
  instance_type          = "t3a.small"
  key_name               = random_pet.ssh_key.id
  monitoring             = true
  vpc_security_group_ids = [module.vault_sg.security_group_id]
  subnet_id              = module.vpc.private_subnets[0]
  enable_volume_tags     = false
  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp3"
      throughput  = 200
      volume_size = 30
    },
  ]

  user_data            = data.template_file.vault_userdata.rendered
  iam_instance_profile = aws_iam_instance_profile.vault_instance_profile.name

}

data "template_file" "vault_userdata" {
  template = file("./userdata.sh")
  vars = {
    kerberos_domain                = upper(aws_directory_service_directory.ms_ad.name)
    domain_dn                      = local.directory_dn
    ad_fqdn                        = aws_directory_service_directory.ms_ad.name
    vault_service_account_user     = local.vault_service_account_user,
    vault_service_account_password = local.vault_service_account_password
    account_id = data.aws_caller_identity.current.account_id
  }
}

resource "aws_route53_record" "vault" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "vault.fcal-vault-demo.example.com"
  type    = "A"
  records = [module.vault_ec2.private_ip[0]]
  ttl     = 300
}

data "aws_iam_policy_document" "vault_instance_trust_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "vault_instance_policy" {
  statement {
    actions = [
      "sts:AssumeRole"
      ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "vault-insance-role" {
  name               = "vault-instance-role"
  assume_role_policy = data.aws_iam_policy_document.vault_instance_trust_policy.json
}

resource "aws_iam_instance_profile" "vault_instance_profile" {
  name = "vault-insance-role"
  role = aws_iam_role.vault-insance-role.name
}

resource "random_password" "vault_service_account_password" {
  length           = 20
  special          = true
  upper            = true
  number           = true
  override_special = "_%@"
}

locals {
  vault_service_account_user     = "svc-fcal-vault"
  vault_service_account_password = random_password.vault_service_account_password.result
}

resource "aws_secretsmanager_secret" "vault_service_account" {
  # checkov:skip=CKV_AWS_149: CMK is fun for another time
  name_prefix = "vault_service_account"
}

resource "aws_secretsmanager_secret_version" "vault_service_account" {
  secret_id = aws_secretsmanager_secret.vault_service_account.id
  secret_string = jsonencode({
    vault_service_account_user     = local.vault_service_account_user,
    vault_service_account_password = local.vault_service_account_password
  })
}