<powershell>

Install-WindowsFeature RSAT-AD-PowerShell
Install-WindowsFeature RSAT-AD-Tools
Install-WindowsFeature RSAT-DNS-Server

$ProgressPreference = 'SilentlyContinue'
wget https://github.com/microsoft/terminal/releases/download/v1.12.10393.0/Microsoft.WindowsTerminal_1.12.10393.0_8wekyb3d8bbwe.msixbundle -OutFile Microsoft.WindowsTerminal_1.12.10393.0_8wekyb3d8bbwe.msixbundle

Add-AppxPackage Microsoft.WindowsTerminal_1.12.10393.0_8wekyb3d8bbwe.msixbundle

New-Item -Path "c:\" -Name "fcal" -ItemType "directory"

$svc_account_script = @"
Import-Module ActiveDirectory
# Create the service account
$svc_account_password = ConvertTo-SecureString "${vault_service_account_password}" -AsPlainText -Force
New-ADUser ${vault_service_account_user} -Path '${domain_dn}' -AccountPassword $svc_account_password -Enabled $true
Set-ADUser ${vault_service_account_user} -Replace @{"msDS-SupportedEncryptionTypes"=($encTypes -bor $AES256)}
# Get the KVNO for Vault Server config step. Each time the password changes, the KVNO will change and the Vault keytab will need to be regenerated
Get-ADUser ${vault_service_account_user} -property msDS-KeyVersionNumber
Set-ADComputer -Identity ${vault_service_account_user} -ServicePrincipalNames -ServicePrincipalNames @{Add='HTTP/vault.fcal-vault-demo.example.com:8200'}
"@

Set-Content -Path "c:\fcal\create_svc_account.ps1" -Value $svc_account_script

</powershell>