resource "random_password" "ad_admin" {
  length      = 16
  special     = true
  min_lower   = 1
  min_upper   = 1
  min_numeric = 1
  min_special = 1
}
resource "aws_directory_service_directory" "ms_ad" {
  name     = "workspaces.infra.fluffycloudsandlines.blog"
  password = random_password.ad_admin.result
  edition  = "Standard"
  type     = "MicrosoftAD"

  vpc_settings {
    vpc_id     = module.vpc.vpc_id
    subnet_ids = module.vpc.intra_subnets
  }

}

resource "aws_workspaces_ip_group" "lab" {
  name        = "Demo"
  description = "Workspaces Demo Lab"

  rules {
    source = var.remote_source_ip
    description = "Lab"
  }
}

resource "aws_workspaces_directory" "wks_registration" {
  directory_id = aws_directory_service_directory.ms_ad.id
  subnet_ids   = module.vpc.private_subnets

  self_service_permissions {
    change_compute_type  = true
    increase_volume_size = true
    rebuild_workspace    = true
    restart_workspace    = true
    switch_running_mode  = true
  }

  workspace_access_properties {
    device_type_android    = "ALLOW"
    device_type_chromeos   = "ALLOW"
    device_type_ios        = "ALLOW"
    device_type_osx        = "ALLOW"
    device_type_linux      = "ALLOW"
    device_type_web        = "DENY"
    device_type_windows    = "ALLOW"
    device_type_zeroclient = "ALLOW"
  }

  workspace_creation_properties {
    custom_security_group_id = module.workspaces_sg.security_group_id
    #default_ou                          = local.directory_dn
    enable_internet_access              = true
    enable_maintenance_mode             = true
    user_enabled_as_local_administrator = true
  }

  depends_on = [
    aws_iam_role_policy_attachment.workspaces_default_service_access,
    aws_iam_role_policy_attachment.workspaces_default_self_service_access,
    module.workspaces_sg
  ]
}


data "aws_iam_policy_document" "workspaces" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["workspaces.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "workspaces_default" {
  name               = "workspaces_DefaultRole"
  assume_role_policy = data.aws_iam_policy_document.workspaces.json
}

resource "aws_iam_role_policy_attachment" "workspaces_default_service_access" {
  role       = aws_iam_role.workspaces_default.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonWorkSpacesServiceAccess"
}

resource "aws_iam_role_policy_attachment" "workspaces_default_self_service_access" {
  role       = aws_iam_role.workspaces_default.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonWorkSpacesSelfServiceAccess"
}

module "workspaces_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "workspaces"
  description = "Security group for workspaces"
  vpc_id      = module.vpc.vpc_id

  egress_with_cidr_blocks = [
    {
      from_port   = -1
      to_port     = -1
      protocol    = "-1"
      description = "workspaces egress"
    },{
      rule        = "https-443-tcp"
      cidr_blocks = "0.0.0.0/0"
      description = "Internet Access"
    },{
      rule        = "http-80-tcp"
      cidr_blocks = "0.0.0.0/0"
      description = "Internet Access"
    },
  ]

  egress_cidr_blocks = var.intra_subnet_cidrs

}

resource "aws_iam_role" "ec2-ssm-role" {
  name               = "windows-ec2-ssm-role"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Service": "ec2.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ssm-instance" {
  role       = aws_iam_role.ec2-ssm-role.id
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role_policy_attachment" "ssm-ad" {
  role       = aws_iam_role.ec2-ssm-role.id
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMDirectoryServiceAccess"
}

resource "aws_iam_instance_profile" "ec2-ssm-role-profile" {
  name = "ec2-ssm-role-profile"
  role = aws_iam_role.ec2-ssm-role.name
}

module "windows_ec2" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 2.0"

  name           = "windows-bastion"
  instance_count = 1

  ami                         = data.aws_ami.windows.id
  instance_type               = "t3a.medium"
  key_name                    = random_pet.ssh_key.id
  monitoring                  = true
  vpc_security_group_ids      = [module.rdp_test_sg.security_group_id]
  subnet_id                   = module.vpc.public_subnets[0]
  associate_public_ip_address = true
  get_password_data           = true
  enable_volume_tags          = false
  iam_instance_profile        = aws_iam_instance_profile.ec2-ssm-role-profile.name
  user_data                   = data.template_file.windows_userdata.rendered

  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp3"
      throughput  = 200
      volume_size = 50
    },
  ]

}

data "template_file" "windows_userdata" {
  template = file("./windows_userdata.ps1")
  vars = {
    ad_path                        = module.vault_ec2.private_ip[0]
    domain_dn                      = local.directory_dn
    ad_fqdn                        = aws_directory_service_directory.ms_ad.name
    vault_service_account_user     = local.vault_service_account_user,
    vault_service_account_password = local.vault_service_account_password
    domain_password                = random_password.ad_admin.result
  }
}

resource "aws_ssm_document" "ssm_document" {
  name          = "joinAD"
  document_type = "Command"
  content       = <<DOC
{
    "schemaVersion": "1.0",
    "description": "Automatic Domain Join Configuration",
    "runtimeConfig": {
        "aws:domainJoin": {
            "properties": {
                "directoryId": "${aws_directory_service_directory.ms_ad.id}",
                "directoryName": "workspaces.infra.fluffycloudsandlines.blog",
                "dnsIpAddresses": ${jsonencode(aws_directory_service_directory.ms_ad.dns_ip_addresses)}
            }
        }
    }
}
DOC
}

resource "aws_ssm_association" "associate_ssm" {
  name = aws_ssm_document.ssm_document.name
  targets {
    key    = "InstanceIds"
    values = module.windows_ec2.id
  }
}