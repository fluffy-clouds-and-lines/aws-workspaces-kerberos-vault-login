data "aws_ami" "windows" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["Windows_Server-2022-English-Full-Base-20*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_ami" "al2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_caller_identity" "current" {}

variable "vpc_cidr" { default = "192.168.96.0/20" }
variable "public_subnet_cidrs" { default = ["192.168.100.0/24", "192.168.101.0/24"] }
variable "intra_subnet_cidrs" { default = ["192.168.102.0/24", "192.168.103.0/24"] }
variable "private_subnet_cidrs" { default = ["192.168.104.0/24", "192.168.105.0/24"] }
variable "remote_source_ip" {}

locals {
  directory_dn = "OU=workspaces,DN=${join(",DN=", split(".", aws_directory_service_directory.ms_ad.name))}"
}