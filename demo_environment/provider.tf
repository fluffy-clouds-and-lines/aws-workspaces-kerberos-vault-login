provider "aws" {
  region = "eu-west-2"
  default_tags {
    tags = {
      Environment = "fcal-vault-demo"
      Owner       = "Me"
      Project     = "Kerberos Login to Vault"
      Terraform   = true
    }
  }
} 