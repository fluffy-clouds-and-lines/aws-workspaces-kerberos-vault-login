data "aws_workspaces_bundle" "linux" {
  owner = "AMAZON"
  name  = "Value with Amazon Linux 2"
}

data "aws_kms_key" "workspaces_aws_managed" {
  key_id = "alias/aws/workspaces"
}

resource "aws_workspaces_workspace" "example" {
  directory_id = aws_directory_service_directory.ms_ad.id
  bundle_id    = data.aws_workspaces_bundle.linux.id
  user_name    = "Admin"

  root_volume_encryption_enabled = true
  user_volume_encryption_enabled = true
  volume_encryption_key          =  data.aws_kms_key.workspaces_aws_managed.arn

  workspace_properties {
    compute_type_name                         = "VALUE"
    user_volume_size_gib                      = 10
    root_volume_size_gib                      = 80
    running_mode                              = "AUTO_STOP"
    running_mode_auto_stop_timeout_in_minutes = 60
  }
}